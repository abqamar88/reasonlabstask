import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:reason_labs/controller/home_controller.dart';
import 'package:reason_labs/utils/common_utils.dart';
import 'package:reason_labs/utils/event_actions.dart';

class HomeScreen extends StatelessWidget{

  HomeController homeController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ReasonLabsTask"),
      ),
      body: Column(
        children: [
          Expanded(
            flex:  20,
            child: Obx((){
              if(homeController.eventsRates.value == EventActions.LOADING){
                return const Center(child: CircularProgressIndicator(),);
              }else if(homeController.eventsRates.value == EventActions.FETCH){
                return Center(child: Text('1 Bitcoin = USD ${homeController.bpiUSD.value.rateFloat}', style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),);
              }else{
               return Container();
              }
            }),
          ),
          Expanded(
            flex: 60,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 15),
              child: Form(
                key: homeController.formKey,
                child: Column(
                  children: [
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      controller: homeController.upperLimitController,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                        TextInputFormatter.withFunction((oldValue, newValue) {
                          try {
                            final text = newValue.text;
                            if (text.isNotEmpty) double.parse(text);
                            return newValue;
                          } catch (e) {}
                          return oldValue;
                        }),
                      ],
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        labelText: "Max rate limit",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        )
                      ),
                      validator: (val) => val.isEmpty ? "required" : null,
                    ),
                    const SizedBox(height: 10,),
                    TextFormField(
                      controller: homeController.lowerLimitController,
                        textInputAction: TextInputAction.done,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                        TextInputFormatter.withFunction((oldValue, newValue) {
                          try {
                            final text = newValue.text;
                            if (text.isNotEmpty) double.parse(text);
                            return newValue;
                          } catch (e) {}
                          return oldValue;
                        }),
                      ],
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                            labelText: "Minimum rate limit",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            )
                        ),
                      validator: (val) => val.isEmpty ? "required" : null,
                    ),
                    const SizedBox(height: 10,),
                    MaterialButton(
                      onPressed: (){
                        if(homeController.formKey.currentState.validate()){
                          homeController.setLimits();
                        }
                      },
                      height: 40,
                      color: Colors.amber,
                      minWidth: Get.width - 10,
                      child: const Text('Set Limit'),
                    )
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 20,
            child: Container(

            ),
          ),
        ],
      ),
    );
  }

}
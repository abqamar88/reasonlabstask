import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:reason_labs/controller/local_notification_controller.dart';
import 'package:reason_labs/http_client/base_client.dart';
import 'package:reason_labs/http_client/base_controller.dart';
import 'package:reason_labs/model/current_rate_response.dart';
import 'package:reason_labs/utils/common_utils.dart';
import 'package:reason_labs/utils/event_actions.dart';
import 'package:reason_labs/utils/storage_manager.dart';
import 'package:workmanager/workmanager.dart';

class HomeController extends GetxController with GetSingleTickerProviderStateMixin, BaseController{

  var bpiEur = BPIData().obs;
  var bpiGBP = BPIData().obs;
  var bpiUSD = BPIData().obs;

  var upperLimitController = TextEditingController();
  var lowerLimitController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  Timer timer;
  var eventsRates = EventActions.NO_DATA.obs;

  final localNotification = Get.put(LocalNotificationController());

    @override
  void onInit() {
    Future.delayed(const Duration(seconds: 5), (){
      timer = Timer.periodic(const Duration(seconds: 15), (timer) {
        callBitCoinRateService();
      });
    });
    callBitCoinRateService();

    isLimitSet().then((value) async{
      if(value){
        upperLimitController.text = await StorageManager.readData(StorageManager.keySelling);
        lowerLimitController.text = await StorageManager.readData(StorageManager.keyPurchase);
        update();
      }
    });

    super.onInit();
  }

  void callBitCoinRateService() async{

      eventsRates.value = EventActions.LOADING;
      var response = await BaseClient().get("http://api.coindesk.com/v1/bpi/currentprice.json").catchError((error){
        //if(timer != null) timer.cancel();
        //CommonUtils.hideLoader();
        handleError(error);
      });

      eventsRates.value = EventActions.FETCH;
      if(response != null && response.bpi != null){
        bpiUSD.value = response.bpi.usd;
        update();


        isLimitSet().then((value){
          if(value){
           if(bpiUSD.value.rateFloat >= double.parse(upperLimitController.text) || bpiUSD.value.rateFloat <= double.parse(lowerLimitController.text)){
             localNotification.showNotification();
             StorageManager.deleteData(StorageManager.keySelling);
             StorageManager.deleteData(StorageManager.keyPurchase);
             StorageManager.deleteData(StorageManager.keyLimitSet);
             upperLimitController.text = '';
             lowerLimitController.text = '';
             update();
             Workmanager().cancelByUniqueName("targetSet");
           }
          }
        });

      }
  }

  void setLimits() async{
      StorageManager.saveData(StorageManager.keySelling, upperLimitController.text);
      StorageManager.saveData(StorageManager.keyPurchase, lowerLimitController.text);
      StorageManager.saveData(StorageManager.keyLimitSet, true);
      Workmanager().registerPeriodicTask("targetSet", "BitCoin Rate Exchange Runner");
  }

  Future<bool> isLimitSet() async{
      return await StorageManager.readDataBool(StorageManager.keyLimitSet);
  }

  @override
  void onClose() {
    if(timer != null) timer.cancel();
    super.onClose();
  }

}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CommonUtils{

  static showLoader(){
    Get.dialog(const Center(child: CircularProgressIndicator(color: Colors.blue,),), barrierDismissible: false);
  }

  static hideLoader(){
    Get.back();
  }

  static void showErrorDialog(message){
    Get.defaultDialog(
      title: "Error",
      middleText: message,
      onConfirm: (){
        Get.back();
      }
    );
  }

  static selectedDates(BuildContext context, bool isPastDate, TextEditingController controller){
    DateTime current = DateTime.now();
    DateTime calendarStart =  DateTime(1950);
    DateTime calendarNextDay = DateTime(current.year, current.month, current.day+1);
    DateTime calendarLastYear = DateTime(current.year+70, current.month, current.day);
    showDatePicker(
      context: context,
      initialDate: isPastDate ? current : calendarNextDay,
      firstDate: isPastDate ? calendarStart : calendarNextDay,
      lastDate:  isPastDate ? DateTime.now() : calendarLastYear,
    ).then((pickedDate){
      if (pickedDate == null) {
        return;
      }else{
        controller.text = DateFormat("yyyy/MM/dd").format(pickedDate);
      }
    });
  }

  static oneMonthDates(BuildContext context, TextEditingController controller){
    DateTime current = DateTime.now();
    DateTime calendarNextDay = DateTime(current.year, current.month, current.day+1);
    DateTime calendarLastDate = DateTime(current.year, current.month+1, current.day);
    showDatePicker(
      context: context,
      initialDate: calendarNextDay,
      firstDate: calendarNextDay,
      lastDate:  calendarLastDate,
    ).then((pickedDate){
      if (pickedDate == null) {
        return;
      }else{
        controller.text = DateFormat("yyyy/MM/dd").format(pickedDate);
      }
    });
  }

  static showSnackBar(String title){
    Get.snackbar('app_name'.tr, title, snackPosition: SnackPosition.BOTTOM, margin: EdgeInsets.only(bottom: 10, right: 10, left: 10));
  }

  static Future<String> selectedDates2(BuildContext context, bool isPastDate) async{
    String selectedDate = '';
    DateTime current = DateTime.now();
    DateTime calendarStart =  DateTime(1950);
    DateTime calendarNextDay = DateTime(current.year, current.month, current.day+1);
    DateTime calendarLastYear = DateTime(current.year+70, current.month, current.day);
    await showDatePicker(
      context: context,
      initialDate: isPastDate ? current : calendarNextDay,
      firstDate: isPastDate ? calendarStart : calendarNextDay,
      lastDate:  isPastDate ? DateTime.now() : calendarLastYear,
    ).then((pickedDate){
      if (pickedDate == null) {
        return;
      }else{
        selectedDate = DateFormat("yyyy-MM-dd").format(pickedDate);
      }
    });
    return selectedDate;
  }

  void showCupertinoDatePicker(BuildContext context, bool isPastDate, TextEditingController controller) {
    DateTime current = DateTime.now();
    DateTime calendarStart =  DateTime(1950);
    DateTime calendarNextDay = DateTime(current.year, current.month, current.day+1);
    DateTime calendarLastYear = DateTime(current.year+70, current.month, current.day);
    showCupertinoModalPopup(
        context: context,
        builder: (_) => Container(
          height: Get.height/2,
          color: Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              Container(
                height: ((Get.height)/2) - 50,
                child: CupertinoDatePicker(
                    //initialDateTime: DateTime.now(),
                    initialDateTime: isPastDate ? current : calendarNextDay,
                    minimumDate: isPastDate ? calendarStart : calendarNextDay,
                    maximumDate:  isPastDate ? DateTime.now() : calendarLastYear,
                    onDateTimeChanged: (val) {
                      controller.text = DateFormat("MM/dd/yyyy").format(val);
                    }),
              ),

              // Close the modal
              CupertinoButton(
                child: Text('ok'.tr),
                onPressed: () => Get.back(),
              )
            ],
          ),
        ));
  }

  static bool isLanguageEnglish(){
    if(Get.locale.languageCode == 'en'){
      return true;
    }
    return false;
  }

  static selectTime(TextEditingController controller){
    showTimePicker(
      initialTime: TimeOfDay.now(),
      context: Get.context,
    ).then((pickedDate){
      if (pickedDate == null) {
        return;
      }else{
        String selectedTime = '${pickedDate.hour}:${pickedDate.minute}';
        controller.text = '${DateFormat("hh:mm a").format(DateFormat("HH:mm").parse(selectedTime))}';
      }
    });
  }

}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

var dropdownDecoration = InputDecoration(
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color:  Colors.grey.withOpacity(0.5), width: 1.0),
    borderRadius: BorderRadius.circular(10.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: const BorderSide(color:  Colors.blue, width: 1.0),
    borderRadius: BorderRadius.circular(10.0),
  ),
  errorBorder: OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.red, width: 1.0),
    borderRadius: BorderRadius.circular(10.0),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color:  Colors.grey.withOpacity(0.5), width: 1.0),
    borderRadius: BorderRadius.circular(10.0),
  ),
);
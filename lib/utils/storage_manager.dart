import 'package:shared_preferences/shared_preferences.dart';

class StorageManager {

  static String keySelling = "key_selling";
  static String keyPurchase = "key_purchase";
  static String keyLimitSet = "key_limit_set";

  static void saveData(String key, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value is int) {
      prefs.setInt(key, value);
    } else if (value is String) {
      prefs.setString(key, value);
    } else if (value is bool) {
      prefs.setBool(key, value);
    } else {

    }
  }

  static Future<String> readData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    String obj = prefs.getString(key) ?? '';
    return obj;
  }

  static Future<bool> readDataBool(String key) async {
    final prefs = await SharedPreferences.getInstance();
    bool obj = prefs.getBool(key) ?? false;
    return obj;
  }

  static Future<bool> deleteData(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }
}
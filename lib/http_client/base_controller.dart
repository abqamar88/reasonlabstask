
import 'package:reason_labs/http_client/app_exception.dart';
import 'package:reason_labs/utils/common_utils.dart';

class BaseController {
  void handleError(error) {
    if (error is BadRequestException) {
      var message = error.message;
      CommonUtils.showErrorDialog(message);
    } else if (error is FetchDataException) {
      var message = error.message;
      CommonUtils.showErrorDialog(message);
    } else if (error is ApiNotRespondingException) {
      CommonUtils.showErrorDialog('Oops! It took longer to respond.');
    }else if (error is UnAuthorizedException){
      CommonUtils.showErrorDialog('Your app token has been expired. Please login again');
    }else{
      CommonUtils.showErrorDialog(error.toString());
    }
  }

  showLoading() {
    CommonUtils.showLoader();
  }

  hideLoading() {
    CommonUtils.hideLoader();
  }
}
import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:reason_labs/http_client/app_exception.dart';
import 'package:reason_labs/model/current_rate_response.dart';

class BaseClient {
  static const int TIME_OUT_DURATION = 60;

  static Map<String, String> header =
  {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "DELETE, POST, GET, PUT",
    "Access-Control-Max-Age": "1728000",
    "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  };

  //GET
  Future<CurrentRateResponse> get(String url_) async {
    var uri = Uri.parse(url_);
    try {
      var response = await http.get(uri).timeout(const Duration(seconds: TIME_OUT_DURATION));
      log(response.body);
      if(response.body != null && response.body.contains('"messageCode": 401')){
        throw UnAuthorizedException(response.body, url_);
      }
      return currentRateResponseFromJson(_processResponse(response));
    } on SocketException {
      throw FetchDataException('no_internet_error'.tr, uri.toString());
    } on TimeoutException {
      throw ApiNotRespondingException('error_while_processing'.tr, uri.toString());
    }
  }

  //POST
  Future<dynamic> post(String url_, dynamic header, dynamic payloadObj) async {
    var uri = Uri.parse(url_);
    var payload = json.encode(payloadObj);
    log(payload);
    try {
      var response = await http.post(uri, headers: header, body: payload).timeout(const Duration(seconds: TIME_OUT_DURATION));
      log(response.body);
      print('${response.statusCode}');
      if(response.body != null && response.body.contains('"messageCode": 401')){
        throw UnAuthorizedException(response.body, url_);
      }
      return currentRateResponseFromJson(_processResponse(response));
    } on SocketException {
      throw FetchDataException('no_internet_error'.tr, uri.toString());
    } on TimeoutException {
      throw ApiNotRespondingException('error_while_processing'.tr, uri.toString());
    }
  }

  //DELETE
  //OTHER

  dynamic _processResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = utf8.decode(response.bodyBytes);
        return responseJson;
        break;
      case 201:
        var responseJson = utf8.decode(response.bodyBytes);
        return responseJson;
        break;
      case 400:
        throw BadRequestException(utf8.decode(response.bodyBytes), response.request.url.toString());
      case 401:
      case 403:
        throw UnAuthorizedException(utf8.decode(response.bodyBytes), response.request.url.toString());
      case 422:
        throw BadRequestException(utf8.decode(response.bodyBytes), response.request.url.toString());
      case 500:
      default:
        throw FetchDataException('Error occured with code : ${response.statusCode}', response.request.url.toString());
    }
  }
}
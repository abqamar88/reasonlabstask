import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reason_labs/controller/home_controller.dart';
import 'package:reason_labs/ui/home_screen.dart';
import 'package:workmanager/workmanager.dart';


void callbackDispatcher() {
  Workmanager().executeTask((task, inputData) {
    final homeController = Get.put(HomeController());
    homeController.isLimitSet().then((value){
      if(!value){
        if(homeController.timer != null) homeController.timer.cancel();
        homeController.onClose();
      }
    });
    return Future.value(true);
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Get.put(HomeController());
  Workmanager().initialize(
      callbackDispatcher,
      isInDebugMode: true
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
